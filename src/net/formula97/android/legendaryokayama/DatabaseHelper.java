/**
 * 
 */
package net.formula97.android.legendaryokayama;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

/**
 * @author HAJIME Fukun(f97one@gmail.com)
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {
	
	private final Context context;
	private String DB_PATH;
	SQLiteDatabase mDatabase;
	
	public DatabaseHelper(Context ctx, String name, CursorFactory factory,
			int version) {
		super(ctx, name, factory, version);

		this.context = ctx;
		
		File dataPath = Environment.getDataDirectory();
		DB_PATH = dataPath.getPath() + "/databases/";
	}
	
	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
		try {
			createEmptyDatabase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			copyDatabaseFromAssets();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	/**
	 * データベースが存在しなければ、空のデータベースファイルを作成する。
	 * @throws IOException
	 */
	public void createEmptyDatabase() throws IOException{
		boolean dbExsits = isDatabaseExsits();
		
		if (dbExsits) {
			// すでにデータベースが存在する
		} else {
			this.getReadableDatabase();
			copyDatabaseFromAssets();
		}
	}

	/**
	 * assetsから出来合いのデータベースファイルをプロダクション環境に書き込む。
	 * @throws IOException
	 */
	private void copyDatabaseFromAssets() throws IOException {
		// TODO Auto-generated method stub
		
		InputStream stream = context.getAssets().open(PConst.Databases.DATABASE_NAME_ASSETS);
		String destFilename = DB_PATH + PConst.Databases.DATABASE_NAME;
		OutputStream outputStream = new FileOutputStream(destFilename);
		
		// ファイルコピー
		byte[] buffer = new byte[1024];
		int size;
		while ((size = stream.read(buffer)) > 0) {
			outputStream.write(buffer, 0, size);
		}
		
		// ストリームを閉じる
		outputStream.flush();
		outputStream.close();
		stream.close();
	}

	/**
	 * コピーしようとしているデータベースファイルが既に存在しているかどうかを確認する。
	 * @return boolean型、存在している場合{@code true}
	 */
	private boolean isDatabaseExsits() {
		// TODO Auto-generated method stub
		SQLiteDatabase checkDb = null;
		
		try {
			String dbPath = DB_PATH + PConst.Databases.DATABASE_NAME;
			checkDb = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READONLY);
		} catch (SQLiteException e) {
			// データベースはまだ存在していない
		}
		
		if (checkDb != null) {
			checkDb.close();
		}
		return checkDb != null ? true : false;
	}
	
	/**
	 * 現在登録されている文書の総数を返す。
	 * @return int型、DBに登録されている文書の総数
	 */
	public int getArticleSum() {
		SQLiteDatabase db = getReadableDatabase();
		Cursor q = db.query("MAINTABLE", null, null, null, null, null, null);
		q.moveToFirst();
		
		int ret = q.getCount();

		db.close();
		return ret;
	}

	/**
	 * データベースから、プライマリキーに該当する緯度経度を返す。
	 * @param primaryKey int型、検索するプライマリキー
	 * @return float[]型、緯度、経度の順の配列
	 */
	public float[] getLatLong(int primaryKey) {
		SQLiteDatabase db = getReadableDatabase();
		
		String[] columns = {"latitude", "longitude"};
		String selection = "_id";
		String[] selectionArgs = {String.valueOf(primaryKey)};
		Cursor q = db.query("MAINTABLE", columns, selection, selectionArgs, null, null, null);
		q.moveToFirst();
		
		float[] ret = { q.getFloat(0), q.getFloat(1) };
		return ret;
	}

}
