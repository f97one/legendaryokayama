/**
 *
 */
package net.formula97.android.legendaryokayama;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.googlecode.androidannotations.annotations.EService;
import com.googlecode.androidannotations.annotations.UiThread;

/**
 * @author HAJIME Fukuna (f97one@gmail.com)
 *
 */
@EService
public class WidgetUpdateService extends Service implements Runnable {

	private static final String ACTION_CLICK = "net.formula97.android.legendaryokayama.ACTION_CLICK";

	Thread thread = null;
	private boolean isRunning = true;
	
	/*
	 * (非 Javadoc)
	 *
	 * @see android.app.Service#onStartCommand(android.content.Intent, int, int)
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO 自動生成されたメソッド・スタブ
		super.onStartCommand(intent, flags, startId);

		// 以下、AppWidgetをタップしたときの処理
		if (ACTION_CLICK.equals(intent.getAction())) {
			// Google検索のキーを引き渡してWebブラウザをstartActivityする
			// remoteViews.setString(R.id.btn_momoslines,
			// "showGoogleSearchResult", "");
			showGoogleSearchResult();
		}
		
		// Threadで位置情報の測位を開始する
		thread = new Thread(this);
		thread.start();

		return START_STICKY_COMPATIBILITY;
	}

	/**
	 * AppWidgetに表示されている文面に対応するキーから、Google検索をstartActivityする。
	 */
	@UiThread
	public void showGoogleSearchResult() {
		// TODO 自動生成されたメソッド・スタブ
		Log.d("showGoogleSearchResult", "Message area tapped.");

		// ブラウザにGoogle検索のintentを飛ばす
		Intent newSite = new Intent(Intent.ACTION_VIEW, buildSearchURL());
		newSite.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		NetUtils utils = new NetUtils(this);
		if (utils.isConnectedToNetwork()) {
			startActivity(newSite);
		}
	}

	/**
	 * Google検索のURLを組み立てる。
	 * @return Uri型、Google検索のパラメータを組み立てたUriオブジェクト
	 */
	private Uri buildSearchURL() {
		SharedPreferences sp = getSharedPreferences(PConst.prefs.PREF_NAME, Context.MODE_PRIVATE);
		String[] searchKey = getResources().getStringArray(R.array.widget_search_key);
		NetUtils netUtils = new NetUtils();
		
		String key = searchKey[sp.getInt(PConst.prefs.CURRENT_SHOWING_MSG_ID, -1)];
		
		Uri uri = Uri.parse(PConst.GOOGLE_BASE_URL + netUtils.spaceToPlus(key));
		return uri;
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(Intent intent) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}


	/*
	 * (非 Javadoc)
	 *
	 * @see android.app.Service#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO 自動生成されたメソッド・スタブ
		super.onDestroy();
		Log.d("WidgetUpdateService#onDestroy",
				"called onDestoroy,stopped service.");
		setStopFlag();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		LocationSearch locationSearch = new LocationSearch(this);
		long duration = locationSearch.getRealScanIntervalFromPreference();
		
		Looper.prepare();
		
		while (isRunning) {
			locationSearch.setUpPositioning();
			try {
				Thread.sleep(duration);
				Log.d("Runnable#run", "Thread slept in " + String.valueOf(duration)
						+ "ms. (" + String.valueOf(duration / 1000 / 60)
						+ " min.)");
				Looper.loop();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 停止フラグをセットしてrun()を素通りさせる。
	 */
	public void setStopFlag() {
		isRunning = false;
	}
}