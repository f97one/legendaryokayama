/**
 * ネットワーク接続に関するユーティリティクラス
 */
package net.formula97.android.legendaryokayama;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author HAJIME Fukuna(f97one@gmail.com)
 *
 */
public class NetUtils {

	Context context;

	/**
	 * 引数を取らないコンストラクタ。Contextが不要な場合に使うので、特に何もしない。
	 */
	public NetUtils() {

	}

	/**
	 * 明示的コンストラクタ。
	 * @param context Context型、ネットワーク接続を判断するActivity自身のContext（呼び出し側でthisを書く）。
	 */
	public NetUtils(Context context) {
		this.context = context;
	}
	/**
	 * 現在何らかのネットワークに接続されているか否かを判断する。
	 * @return boolean型、接続されていればtrue、接続されていなければfalseを返す。
	 */
	public boolean isConnectedToNetwork() {
		boolean ret;
		ConnectivityManager mConnectmgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo nwInfo = mConnectmgr.getActiveNetworkInfo();

		if (nwInfo == null) {
			// 有効なネットワーク自体がない場合は、falseとする
			ret = false;
		} else {
			ret = nwInfo.isConnected();
		}

		return ret;
	}

	/**
	 * Google検索キーワード用に、1つ以上連続する半角スペースを「+」に置き換える。<br>
	 * ただし、文字列の先頭にある「連続する半角スペース」は、置換の対象外とする。
	 * @param keyWords String型、検索キーワードに使う文字列
	 * @return 1つ以上連続する半角スペースを「+」に置換した結果
	 */
	public String spaceToPlus(String keyWords) {
		String working;
		String whiteSpaces = " +";
		String headWhiteSpaces = "^" + whiteSpaces;
		String replacement = "+";

		// 行中にある全角スペースを半角スペースに置換
		working = keyWords.replaceAll("　", " ");
		
		// 行頭の「連続する半角スペース」を削除する
		//   一致しない場合は何もしなくてもよいので、workingの中身はそのまま
		Pattern pattern = Pattern.compile(headWhiteSpaces);
		Matcher matcher = pattern.matcher(working);
		if (matcher.find()) {
			working = working.replaceAll(headWhiteSpaces, "");
		}
		
		// 行中にある「連続する半角スペース」を、「半角スペース×１」に置換
		working = working.replaceAll(whiteSpaces, " ");
		
		// 最終的に半角スペースを「+」に置換
		return working.replaceAll(" ", replacement);
	}
}
