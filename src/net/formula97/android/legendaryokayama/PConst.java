/**
 *
 */
package net.formula97.android.legendaryokayama;

/**
 * このプロジェクトで使用する定数値の集合。
 * @author HAJIME Fukuna (f97one@gmail.com)
 */
public final class PConst {

	public static final String OFFICIAL_WEB = "http://den-oka.jp/";
	public static final String GOOGLE_BASE_URL = "http://www.google.com/search?ie=utf-8&q=";

	public final class prefs {
		public static final String PREF_NAME = "LegendaryOkayama_preference";
		public static final String REFRESH_INTERVAL = "RefreshInterval";
		public static final String LOCATION_SCAN_INTERVAL = "LocationScanInterval";
		public static final String CURRENT_SHOWING_MSG_ID = "CurrentShowingMsgId";

		public static final int INITIAL_REFRESH_INTERVAL = 5;

		public static final int INITIAL_LOCATION_SCAN_INTERVAL = prefs.scan.INTERVAL_3HOUR;

		public final class scan {
			public static final int INTERVAL_15MIN = 1;
			public static final int INTERVAL_30MIN = 2;
			public static final int INTERVAL_1HOUR = 3;
			public static final int INTERVAL_3HOUR = 4;
			public static final int INTERVAL_6HOUR = 5;

			public static final int INTERVAL_15MIN_REAL = 15;
			public static final int INTERVAL_30MIN_REAL = 30;
			public static final int INTERVAL_1HOUR_REAL = 1;
			public static final int INTERVAL_3HOUR_REAL = 3;
			public static final int INTERVAL_6HOUR_REAL = 6;

		}
		
		public static final String CURRENT_LATITUDE = "CurrentLatitude";
		public static final String CURRENT_LONGITUDE = "CurrentLongitude";
		
		public static final int SPOTTING_RANGE_IN_METER = 3000;
		
	}
	
	public final class Databases {
		public static final String DATABASE_NAME_ASSETS = "locationHolder.db";
		public static final String DATABASE_NAME = "locationHolder";
		public static final int DATABASE_VERSION = 1;
	}
}
