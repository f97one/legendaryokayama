package net.formula97.android.legendaryokayama;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.location.Location;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * AppWidget表示に関するクラス。<br />
 * AlarmManagerを毎回インスタンス化しているが、フィールドではなくこうしておかないと
 * 意図通りサービスを停止してくれないので、注意のこと。
 * @author HAJIME Fukuna (f97one@gmail.com)
 *
 */
public class WidgetView extends AppWidgetProvider {

	private static final String ACTION_CLICK = "net.formula97.android.legendaryokayama.ACTION_CLICK";
	
	// service起動監視のための正規名
	private static final String mServiceName = WidgetUpdateService_.class.getCanonicalName();
	
	/**
	 * WidgetUpdateService開始処理
	 * @param context Context型、アプリケーションコンテキスト
	 * @param appWidgetManager AppWidgetManager型、AppWidgetを操作するManagerクラスのインスタンス
	 * @param appWidgetIds int[]型、サービス開始をセットするAppWidgetのid
	 */
	private void callStartService(Context context,
			AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// ANR防止のため、サービス経由でAppWidgetの更新を行う
		Intent callSvc = new Intent(context, WidgetUpdateService_.class);
		ComponentName componentName = context.startService(callSvc);
		Log.d("WidgetView#onUpdate", "called service.");
		if (TextUtils.isEmpty(componentName.getShortClassName())) {
			Log.w("WidgetView#onUpdate", "failed to start service.");
		} else {
			Log.d("WidgetView#onUpdate", "service [" + componentName.getShortClassName() + "] started successfully.");
		}
	}

	/**
	 * ACTION_APPWIDGET_UPDATEブロードキャストを投げるPendingIntentを定義する。
	 * @param context Context型、アプリケーションコンテキスト
	 * @return PendingIntent型、ブロードキャストを投げるPendingIntentオブジェクト
	 */
	private PendingIntent getPendingAlarmIntent(Context context) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(context, WidgetView.class);
		intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		return pendingIntent;
	}

	/**
	 * WidgetUpdateServiceが既に起動しているか否かを返す。
	 * @param context Context型、アプリケーションコンテキスト
	 * @return 起動している場合はtrue、そうでない場合はfalseを返す
	 */
	private boolean isServiceRunning(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);
		
		for (RunningServiceInfo info : services) {
			if (mServiceName.equals(info.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * String-Arrayに保管しているメッセージのうち、ランダムな添え字の値を返す。
	 * @param context Context型、アプリケーションコンテキスト
	 * @return int型、ランダムな添え字の値
	 */
	private int makeRandomId(Context context) {
		Resources res = context.getResources();
		int resTotal = res.getStringArray(R.array.widget_message_body).length;

		Random rnd = new Random();
		
		return rnd.nextInt(resTotal);
	}

	/**
	 * ArrayListに保持されているメッセージIDのうち、ランダムな添え字の値を返す。
	 * @param nearVenuesId ArrayList<Integer>型、
	 * @return int型、ランダムな添え字の値
	 */
	private int makeRandomId(ArrayList<Integer> nearVenuesId) {
		int alSize = nearVenuesId.size();

		Random rnd = new Random();
		
		return nearVenuesId.get(rnd.nextInt(alSize));
	}

	/* (非 Javadoc)
	 * @see android.appwidget.AppWidgetProvider#onDeleted(android.content.Context, int[])
	 */
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		super.onDeleted(context, appWidgetIds);
		// AppWidgetが削除されたら、サービスを停止する
		Intent callSvc = new Intent(context, WidgetUpdateService_.class);
		boolean result = context.stopService(callSvc);
		if (result) {
			Log.d("WidgetView#onDeleted", "service stopped successfully.");
		} else {
			Log.w("WidgetView#onDeleted", "failed to stop service(already stopped?).");
		}
	}

	/**
	 * AppWidgetをすべて削除した時の処理。<br />
	 * AlarmManagerを停止して、AppWidgetの更新監視をやめる。
	 * @param context
	 * @see android.appwidget.AppWidgetProvider#onDisabled(android.content.Context)
	 */
	@Override
	public void onDisabled(Context context) {
		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent pendingIntent = getPendingAlarmIntent(context);
		manager.cancel(pendingIntent);
		
		super.onDisabled(context);
	}

	/* (non-Javadoc)
	 * @see android.appwidget.AppWidgetProvider#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		String receivedAction = intent.getAction();
		if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(receivedAction)) {
			setAlarm(context);
			setMessages(context);
		}
		// TODO Auto-generated method stub
		super.onReceive(context, intent);
	}

	/* (非 Javadoc)
	 * @see android.appwidget.AppWidgetProvider#onUpdate(android.content.Context, android.appwidget.AppWidgetManager, int[])
	 */
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// サービスが起動していなければ、サービスの起動にかかる
		if (!isServiceRunning(context)) {
			callStartService(context, appWidgetManager, appWidgetIds);

			// クリック処理を定義する
			setClickPendingIntent(context, appWidgetManager, appWidgetIds);
			
		}

		super.onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	/**
	 * AlarmManagerに次回のイベント発生時間を定義する。
	 * @param context Context型、アプリケーションコンテキスト
	 */
	private void setAlarm(Context context) {
		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		// 次回の起動タイミングをPreferenceから読み取った値にセットする
		PackageManager packageManager = context.getPackageManager();
		ApplicationInfo info = null;
		
		// PreferenceからAppWidgetの更新間隔を取得する
		SharedPreferences preferences = context.getSharedPreferences(
				PConst.prefs.PREF_NAME, Context.MODE_PRIVATE);
		int refreshInterval = makeDuration(preferences);
		Log.d("setAlarm", "Read from Preference is " + String.valueOf(refreshInterval) + " min. (5 sec. fixed if debuggable is ture)");

		// デバッグ実行中か否かを判断
		try {
			info = packageManager.getApplicationInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();

		// デバッグ実行中なら、更新間隔を５秒に固定するが、
		// リリース実行中ならPreferenceの値（単位：分）だけ待つ
		if ((info.flags & ApplicationInfo.FLAG_DEBUGGABLE) == ApplicationInfo.FLAG_DEBUGGABLE) {
			cal.add(Calendar.SECOND, 5);
		} else {
			cal.add(Calendar.MINUTE, refreshInterval);
		}
		PendingIntent intent = getPendingAlarmIntent(context);
		manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), intent);
	}
	
	/**
	 * クリック処理をセットする。
	 * @param context Context型、アプリケーションコンテキスト
	 * @param appWidgetManager AppWidgetManager型、AppWidgetを操作するManagerクラスのインスタンス
	 * @param appWidgetIds int[]型、クリック処理をセットするAppWidgetのid
	 */
	private void setClickPendingIntent(Context context,
			AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// 以下、AppWidgetをタップしたときの処理として、pendingIntentを定義
		// 実際のクリック処理は、Service内部で行う。
		Intent tapped = new Intent(context, WidgetUpdateService_.class);
		tapped.setAction(ACTION_CLICK);
		PendingIntent pendingIntent = PendingIntent.getService(context, 0, tapped, 0);
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_main);
		remoteViews.setOnClickPendingIntent(R.id.btn_momoslines, pendingIntent);
		appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
	}
	
	/**
	 * AppWidgetにメッセージをセットする。
	 * @param context Context型、アプリケーションコンテキスト
	 */
	private void setMessages(Context context) {
		SharedPreferences sp = context.getSharedPreferences(PConst.prefs.PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sp.edit();
		
		// 表示するメッセージの添え字を取得する
		int showing;
		ArrayList<Integer> venues = new ArrayList<Integer>();
		venues = getNearVenues(context);
		
		if (showNearVenue() && !venues.isEmpty()) {
			showing = makeRandomId(venues);
			Log.v("setMessage", "determined nearby spot with reference to GPS, MsgId = " + String.valueOf(showing));
		} else {
			showing = makeRandomId(context);
		}
		Log.d("setMessages", "Next showing message : ID = " + String.valueOf(showing));
		
		// XMLの配列から、
		Resources resources = context.getResources();
		String[] text = resources.getStringArray(R.array.widget_message_body);
		Log.d("setMessages", "Next showing message : Message = " + text[showing]);

		// AppWidgetにメッセージをセットして再描画
		RemoteViews layout = new RemoteViews(context.getPackageName(), R.layout.widget_main);
		layout.setTextViewText(R.id.btn_momoslines, text[showing]);
	
		ComponentName name = new ComponentName(context, WidgetView.class);
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		appWidgetManager.updateAppWidget(name, layout);

		editor.putInt(PConst.prefs.CURRENT_SHOWING_MSG_ID, showing);
		editor.commit();
	}
	
	/**
	 * 位置情報から近隣のスポット一覧を返す。
	 * @param context Context型、アプリケーションコンテキスト
	 * @return ArayList<Integer>型、3000m以内にあるスポットIDの配列
	 */
	private ArrayList<Integer> getNearVenues(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(
				PConst.prefs.PREF_NAME, Context.MODE_PRIVATE);

		Resources res = context.getResources();
		int resTotal = res.getStringArray(R.array.widget_message_body).length;
		TypedArray lat = res.obtainTypedArray(R.array.latituide);
		TypedArray lng = res.obtainTypedArray(R.array.longitude);

		long currentLat = preferences.getLong(PConst.prefs.CURRENT_LATITUDE, 0);
		long currentLng = preferences.getLong(PConst.prefs.CURRENT_LONGITUDE, 0);
		
		ArrayList<Integer> ret = new ArrayList<Integer>();
		
		for (int i = 0; i < resTotal; i++) {
			float[] results = new float[3];
			Location.distanceBetween(currentLat, currentLng, (long)lat.getFloat(i, 0), (long)lng.getFloat(i, 0), results);
			
			if (results[0] <= (float)PConst.prefs.SPOTTING_RANGE_IN_METER) {
				ret.add(i);
			}
		}
		
		// SDKの解説に従い、TypedArray#recycle()をコールして再利用可とする
		lat.recycle();
		lng.recycle();
		return ret;
	}
	
	/**
	 * 近隣スポットを表示するか否かを決める。
	 * @return boolean型、表示するならtrue、しないならfalseを返す。trueとfalseは1:3の割合。
	 */
	private boolean showNearVenue() {
		Random rnd = new Random();
		return rnd.nextInt(4) == 0 ? true : false;
	}
	
	/**
	 * 更新間隔を計算する。
	 * @param preference SharedPreference型、保管されているPreference
	 * @return int型、Preferenceに保管されている更新間隔を返すが、これが0の場合は、5〜30分の間でランダムな値を返す。
	 */
	private int makeDuration(SharedPreferences preferences) {
		int ret;
		int refresh = preferences.getInt(PConst.prefs.REFRESH_INTERVAL, 5);
		Random rnd = new Random();

		if (refresh == 0) {
			ret = rnd.nextInt(26) + 5;
		} else {
			ret = refresh;
		}
		
		Log.v("makeDureation", "Widget refresh duration is " + String.valueOf(ret) +"min.");
		return ret;
	}
}
