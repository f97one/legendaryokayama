package net.formula97.android.legendaryokayama;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;

/**
 * アプリケーションをランチャーから起動した場合に、設定項目を表示するActivity。
 *
 * @author HAJIME Fukuna (f97one@gmail.com)
 */
@EActivity
public class MainActivity extends Activity implements OnSeekBarChangeListener {

	SharedPreferences sp;

	@ViewById(R.id.seekBar_refreshInterval)
	SeekBar seekBar_refreshInterval;
	@ViewById(R.id.seekBar_locationScanInterval)
	SeekBar seekBar_locationScanInterval;
	@ViewById(R.id.tv_refreshValues)
	TextView tv_refreshValues;
	@ViewById(R.id.tv_locationScanValue)
	TextView tv_locationScanValue;
	@ViewById(R.id.button_increaseRefresh)
	Button button_inreaseRefresh;
	@ViewById(R.id.button_decreaseRefresh)
	Button button_decreaseRefresh;
	@ViewById(R.id.button_increaseScan)
	Button button_increaseScan;
	@ViewById(R.id.button_decreaseScan)
	Button button_decreaseScan;

	/*
	 * (非 Javadoc)
	 *
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		sp = getSharedPreferences(PConst.prefs.PREF_NAME,
				MODE_PRIVATE);
		
		// Android 3.0未満の場合は、タイトルバーの左にアイコンを表示する
		// requestWindowFeatureは、setContentviewの前に書かないと有効にならないので、
		// わざわざこんな書き方になっている。
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			requestWindowFeature(Window.FEATURE_LEFT_ICON);
		}
		setContentView(R.layout.activity_main);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);
		}
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }

	/*
	 * (非 Javadoc)
	 *
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		// Preferenceへ値を格納する
		SharedPreferences.Editor spe = sp.edit();
		spe.putInt(PConst.prefs.REFRESH_INTERVAL,
				seekBar_refreshInterval.getProgress());
		spe.putInt(PConst.prefs.LOCATION_SCAN_INTERVAL,
				seekBar_locationScanInterval.getProgress());
		spe.commit();
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// SeekBarのイニシャライズ
		seekBar_refreshInterval.setMax(60);
		seekBar_locationScanInterval.setMax(5);

		// 呼び出し直後の値は、Preferenceに保存されている値から呼び出しを試みる
		int currentRefreshInterval = sp.getInt(
				PConst.prefs.REFRESH_INTERVAL,
				PConst.prefs.INITIAL_REFRESH_INTERVAL);
		int currentScanInterval = sp.getInt(
				PConst.prefs.LOCATION_SCAN_INTERVAL,
				PConst.prefs.INITIAL_LOCATION_SCAN_INTERVAL);

		seekBar_refreshInterval.setProgress(currentRefreshInterval);
		seekBar_locationScanInterval.setProgress(currentScanInterval);

		// Preferenceから読みだした値でSeekBar周りをセットする
		onProgressChanged(seekBar_refreshInterval, currentRefreshInterval,
				false);
		onProgressChanged(seekBar_locationScanInterval, currentScanInterval,
				false);

		// コールバックリスナーを定義
		seekBar_refreshInterval.setOnSeekBarChangeListener(this);
		seekBar_locationScanInterval.setOnSeekBarChangeListener(this);
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * android.widget.SeekBar.OnSeekBarChangeListener#onProgressChanged(android
	 * .widget.SeekBar, int, boolean)
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO 自動生成されたメソッド・スタブ
		if (fromUser) {
			Log.d("onProgressChanged", "initiated by user.");
		} else {
			Log.d("onProgressChanged", "initiated by program.");
		}

		// SeekBarのIDによって、処理を振り分ける
		switch (seekBar.getId()) {
		case R.id.seekBar_refreshInterval:
			setRefreshProgress(progress);
			if (!fromUser) {
				seekBar_refreshInterval.refreshDrawableState();
			}
			break;
		case R.id.seekBar_locationScanInterval:
			setScanProgress(progress);
			if (!fromUser) {
				seekBar_locationScanInterval.refreshDrawableState();
			}
		default:

		}
	}

	/**
	 * 更新間隔のシークバーに対応するラベルの値をセットする。
	 * @param progress int型、シークバーのprogress値。
	 */
	private void setRefreshProgress(int progress) {
		if (progress != 0) {
			tv_refreshValues.setText(String.valueOf(progress)
					+ getString(R.string.tv_minute));
		} else {
			tv_refreshValues.setText(getString(R.string.tv_random));
		}
	}

	/**
	 * 位置スキャン間隔のシークバーに対応するラベルの値セットする。
	 * @param progress int型、シークバーのprogress値。
	 */
	private void setScanProgress(int progress) {
		switch (progress) {
		case PConst.prefs.scan.INTERVAL_15MIN:
			tv_locationScanValue
					.setText(String
							.valueOf(PConst.prefs.scan.INTERVAL_15MIN_REAL)
							+ getString(R.string.tv_minute));
			break;
		case PConst.prefs.scan.INTERVAL_30MIN:
			tv_locationScanValue
					.setText(String
							.valueOf(PConst.prefs.scan.INTERVAL_30MIN_REAL)
							+ getString(R.string.tv_minute));
			break;
		case PConst.prefs.scan.INTERVAL_1HOUR:
			tv_locationScanValue
					.setText(String
							.valueOf(PConst.prefs.scan.INTERVAL_1HOUR_REAL)
							+ getString(R.string.tv_hour));
			break;
		case PConst.prefs.scan.INTERVAL_3HOUR:
			tv_locationScanValue
					.setText(String
							.valueOf(PConst.prefs.scan.INTERVAL_3HOUR_REAL)
							+ getString(R.string.tv_hour));
			break;
		case PConst.prefs.scan.INTERVAL_6HOUR:
			tv_locationScanValue
					.setText(String
							.valueOf(PConst.prefs.scan.INTERVAL_6HOUR_REAL)
							+ getString(R.string.tv_hour));
			break;
		default:
			tv_locationScanValue.setText(getString(R.string.tv_noscan));
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * android.widget.SeekBar.OnSeekBarChangeListener#onStartTrackingTouch(android
	 * .widget.SeekBar)
	 */
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO 自動生成されたメソッド・スタブ

	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * android.widget.SeekBar.OnSeekBarChangeListener#onStopTrackingTouch(android
	 * .widget.SeekBar)
	 */
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO 自動生成されたメソッド・スタブ

	}
	
	/**
	 * 位置スキャン間隔を増加するボタンを押した時の処理を行う。
	 */
	@Click({R.id.button_increaseScan})
	public void button_increaseScan_Click() {
		int current = seekBar_locationScanInterval.getProgress();
		
		// 現在値＋１がプログレスバーの最大値を超えている場合は、最大値にする
		if (seekBar_locationScanInterval.getMax() > current) {
			seekBar_locationScanInterval.incrementProgressBy(1);
			onProgressChanged(seekBar_locationScanInterval, current + 1, false);
		}
	}
	
	/**
	 * 位置スキャン間隔を減少するボタンを押した時の処理を行う。
	 */
	@Click({R.id.button_decreaseScan})
	public void button_decreaseScan_Click() {
		int current = seekBar_locationScanInterval.getProgress();
		
		// 現在値が0でない場合だけ、現在値-1する。
		if (current != 0) {
			seekBar_locationScanInterval.incrementProgressBy(-1);
			onProgressChanged(seekBar_locationScanInterval, current - 1, false);
		}
	}
	
	/**
	 * 更新間隔を増加するボタンを押した時の処理を行う。
	 */
	@Click({R.id.button_increaseRefresh})
	public void button_increaseRefresh_Click() {
		int current = seekBar_refreshInterval.getProgress();
		
		// 現在値＋５がプログレスバーの最大値を超えている場合は、最大値にする
		if (seekBar_refreshInterval.getMax() < current + 5) {
			seekBar_refreshInterval.setProgress(seekBar_refreshInterval.getMax());
		} else {
			seekBar_refreshInterval.incrementProgressBy(5);
		}
		
		int after = seekBar_refreshInterval.getProgress();
		onProgressChanged(seekBar_refreshInterval, after, false);
	}
	
	/**
	 * 更新間隔を減少させるボタンを押した時の処理を行う。
	 */
	@Click({R.id.button_decreaseRefresh})
	public void button_decreaseRefresh_Click() {
		int current = seekBar_refreshInterval.getProgress();
		
		// 現在値が５より大きい時だけ、現在値－５する。
		if (current > 5) {
			seekBar_refreshInterval.incrementProgressBy(-5);
		} else {
			seekBar_refreshInterval.setProgress(0);
		}
		
		int after = seekBar_refreshInterval.getProgress();
		onProgressChanged(seekBar_refreshInterval, after, false);
	}
}
