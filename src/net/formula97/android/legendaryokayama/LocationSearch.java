/**
 * 
 */
package net.formula97.android.legendaryokayama;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

/**
 * 位置情報を取得、管理するためのクラス。
 * @author HAJIME Fukuna(f97one@gmail.com)
 *
 */
public class LocationSearch implements LocationListener {

	private Context context;
	private LocationManager locationManager;
	private SharedPreferences preferences;
	
	/**
	 * コンストラクタ。LocationManagerとPreferenceの初期化を行う。
	 * @param ctx Context型、SystemService取得に必要なアプリケーションコンテキスト
	 */
	public LocationSearch(Context ctx) {
		this.context = ctx;
		
		// Preferenceを読み書きできるようにする
		preferences = context.getSharedPreferences(PConst.prefs.PREF_NAME, Context.MODE_PRIVATE);

		// 位置情報が取得できるようにする
		// システム負荷が高くならないよう、この時点で「最もコストが安い手段」
		// (＝低精度、低消費電力、高度、速度ともに不要)にする
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	}

	/**
	 * 測位を開始する。<br />
	 * 最適な位置情報プロバイダが発見できなかったときは、何もせずに終了する。
	 */
	@SuppressLint("InlinedApi")
	public void setUpPositioning() {
		Criteria criteria = new Criteria();
		// 測位精度の設定APIはLevel 9(=OS 2.3)以上で可能なので、Level 8(=OS 2.2)以下では
		// 測位精度の設定はバイパスする
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
			criteria.setAccuracy(Criteria.ACCURACY_LOW);
		} else {
			criteria.setAccuracy(Criteria.ACCURACY_COARSE);
		}
		criteria.setPowerRequirement(Criteria.POWER_LOW);	// 消費電力
		criteria.setAltitudeRequired(false);				// 高度情報の要求
		criteria.setSpeedRequired(false);					// 速度情報の要求
		String provider = locationManager.getBestProvider(criteria, true);
		Log.d("setUpPositionning", "got privider : " + provider);
		
		long scanInterval = getRealScanIntervalFromPreference();
		Log.d("setUpPositioning", "found Scan Interval : " + String.valueOf(scanInterval) + "ms.");

		// 最適な位置情報プロバイダが見つけられなかったとき、またはスキャン間隔が0の
		// 場合は、リスナーを定義せず終了する。
		if (provider != null && scanInterval != 0) {
			locationManager.requestLocationUpdates(provider, 0, 0,	this);
		}
	}

	/**
	 * Preferenceから位置スキャン間隔を取得して返す。
	 * @return int型、最少スキャン間隔（単位ミリ秒）
	 */
	public long getRealScanIntervalFromPreference() {
		// TODO Auto-generated method stub
		int scanIndex = preferences.getInt(PConst.prefs.LOCATION_SCAN_INTERVAL, PConst.prefs.INITIAL_LOCATION_SCAN_INTERVAL);
		int ret;
		int minuteInMillis = 60 * 1000;
		
		switch (scanIndex) {
		case PConst.prefs.scan.INTERVAL_15MIN:
			ret = PConst.prefs.scan.INTERVAL_15MIN_REAL * minuteInMillis;
			break;
		case PConst.prefs.scan.INTERVAL_30MIN:
			ret = PConst.prefs.scan.INTERVAL_30MIN_REAL * minuteInMillis;
			break;
		case PConst.prefs.scan.INTERVAL_1HOUR:
			ret = PConst.prefs.scan.INTERVAL_1HOUR_REAL * 60 * minuteInMillis;
			break;
		case PConst.prefs.scan.INTERVAL_3HOUR:
			ret = PConst.prefs.scan.INTERVAL_3HOUR_REAL * 60 * minuteInMillis;
			break;
		case PConst.prefs.scan.INTERVAL_6HOUR:
			ret = PConst.prefs.scan.INTERVAL_6HOUR_REAL * 60 * minuteInMillis;
			break;
		default:
			ret = 0;
			break;
		}
		
		return ret;
	}

	/* (non-Javadoc)
	 * @see android.location.LocationListener#onLocationChanged(android.location.Location)
	 */
	@Override
	public void onLocationChanged(Location location) {
		// Preferenceにはdoubleを格納するメソッドが用意されていないため、floatに丸めて
		// 緯度経度をPreferenceに渡す
		float latitude = (float) location.getLatitude();
		float longitude = (float) location.getLongitude();
		
		exportLocation(latitude, longitude);
	}

	/* (non-Javadoc)
	 * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
	 */
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.location.LocationListener#onProviderEnabled(java.lang.String)
	 */
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see android.location.LocationListener#onStatusChanged(java.lang.String, int, android.os.Bundle)
	 */
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 取得した緯度経度を、Preferenceに格納する。
	 * @param latitude float型、現在の緯度
	 * @param longitude float型、現在の経度
	 */
	private void exportLocation(float latitude, float longitude) {
		SharedPreferences.Editor editor = preferences.edit();
		
		editor.putFloat(PConst.prefs.CURRENT_LATITUDE, latitude);
		editor.putFloat(PConst.prefs.CURRENT_LONGITUDE, longitude);
		editor.commit();
	}
	
	/**
	 * 測位を停止する。
	 */
	public void stopPositioning() {
		locationManager.removeUpdates(this);
	}
}
